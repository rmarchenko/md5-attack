#ifndef UTILS_H
#define	UTILS_H

#include <sstream>
#include <string>
#include <vector>

// http://stackoverflow.com/a/236803/465662

std::vector<std::string> &split(const std::string &s,
                                char delim,
                                std::vector<std::string> &elems)
{
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim))
  {
    elems.push_back(item);
  }
  return elems;
}

std::vector<std::string> split(const std::string &s, char delim)
{
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}


#endif	/* UTILS_H */

