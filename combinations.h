#ifndef COMBINATIONS_H
#define	COMBINATIONS_H

#include <algorithm>
#include <string>
#include <vector>

/**
 * Combinations generator.
 * Generates combination strings for the specified elements.
 * The generated string length is varying in range [minLen, maxLen].
 */
class Combinations
{
  const std::string _elements;
  const size_t _minLen;
  const size_t _maxLen;

  std::string _output;
  typedef std::vector<size_t> index_vector;
  index_vector _indexes;
  
  void generate () 
  {
    _output.resize(_indexes.size());
    std::transform(_indexes.begin(), _indexes.end(), _output.begin(),
                   [this] (size_t v) { return this->_elements[v]; });
  }
  
public:

  /**
   * Constructor
   * @param elements elements to combinate
   * @param minLen minimal length of combination
   * @param maxLen maximum length of combination
   */
  Combinations(const char *elements, size_t minLen, size_t maxLen) :
  _elements(elements), _minLen(minLen), _maxLen(maxLen)
  {
    _indexes.reserve(_maxLen);
    _indexes = index_vector(_minLen, 0);
    _output.reserve(_maxLen + 1);
    
    generate();
  }

  /**
   * Get current combination.
   * 
   * @return nullptr if no more combinations, 
   * otherwise returns current combination.
   */
  const char * get() const
  {
    return (_indexes.size() > _maxLen) ? nullptr : _output.c_str();
  }
  
  /**
   * Generates and returns the next combination.
   * The same as "combinations += 1".
   * 
   * @return nullptr if no more combinations, 
   * otherwise returns the next combination.
   */
  const char * next()
  {
    *this += 1;
    return get();
  }
  
  /**
   * Skip n combinations. 
   * @param n 
   */
  Combinations &operator += (size_t n)
  {
    if (_indexes.size() > _maxLen)
    {
      return *this;
    }
    
    size_t v = n;
    size_t i = 0;
    while (0 < v)
    {
      if (_indexes.size() <= i)
      {
        _indexes.push_back(0);
        v -= 1;
      }
      
      _indexes[i] += v;
      size_t rem = _indexes[i] % _elements.size();
      size_t mul = _indexes[i] / _elements.size();
      _indexes[i] = rem;
      v = mul;
      i += 1;
    }
    
    generate();

    return *this;
  }
};

#endif	/* COMBINATIONS_H */

