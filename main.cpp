#include <cassert>
#include <cmath>
#include <iostream>
#include <thread>

#include "concurrent_queue.h"
#include "combinations.h"
#include "md5.h"
#include "utils.h"

typedef std::vector<std::string> string_vector;
typedef std::pair<int, int> int_pair;
typedef std::vector<int_pair> range_vector;

using namespace std;

// -----------------------------------------------------------------------------

const size_t DesiredThreadInterval = 30; // seconds

// -----------------------------------------------------------------------------

struct message
{
  bool found;
  size_t duration;
  size_t attackNum;
};

typedef concurrent_queue<message> message_queue;

struct thread_control
{
  thread_control() : stopThread(false) {}
  message_queue messageQueue;
  bool stopThread;
};

// -----------------------------------------------------------------------------

/**
 * Unit tests.
 */
void test()
{
  {
    MD5 md5;
    assert(string("7f46e34f077fe20a8a4c3190f0bddbf0") == md5.digestString("HELLO THERE I AM MD5!"));
    assert(string("d4b7c284882ca9e208bb65e8abd5f4c8") == md5.digestString("bcd"));
    assert(string("8539ef1fba74a70f5a77fcc3f25c1659") == md5.digestString("BCD"));
  }

  {
    Combinations comb("abcd", 2, 3);
   // Total combinations: 4^2 + 4^3 = 80;
    int total = 80;

    assert(string("aa") == comb.get());
    --total;
    assert(string("ba") == comb.next());
    --total;
    assert(string("ca") == comb.next());
    --total;
    assert(string("da") == comb.next());
    --total;
    assert(string("ab") == comb.next());
    --total;
    assert(string("bb") == comb.next());
    --total;
    assert(string("cb") == comb.next());
    --total;

    while (comb.next())
    {
      --total;
    }
    assert(0 == total);
  }

  {
    Combinations comb("ab", 3, 5);
    // Total combinations: 2^3 + 2^4 + 2^5 = 56;
    int total = 56;

    assert(string("aaa") == comb.get());
    
    comb += 8;
    total -= 8;
    assert(string("aaaa") == comb.get());

    comb += 16;
    total -= 16;
    assert(string("aaaaa") == comb.get());

    comb += 32;
    total -= 32;
    assert(nullptr == comb.get());

    assert(0 == total);
  }

  {
    Combinations comb1("abcd", 2, 2);
    comb1 += 8;
    assert(string("ac") == comb1.get());
    
    Combinations comb2(comb1);
    assert(string("ac") == comb2.get());
    assert(string("ac") == comb1.get());

    comb1 += 2;
    comb2 += 5;
    assert(string("cc") == comb1.get());
    assert(string("bd") == comb2.get());
  }
}

// -----------------------------------------------------------------------------

void attack_thread (const std::string &strHash, 
                    Combinations comb, 
                    const size_t attackNum,
                    void *param)
{
  using namespace std::chrono;
 
  // There is a compiling issue if message_queue passed as a param,
  // so I'm using void* here.
  thread_control *threadControl = static_cast<thread_control *>(param);

  const std::string start(comb.get());
  const system_clock::time_point t0 = system_clock::now();
  MD5 md5;
  
  size_t n = attackNum;
  while (!threadControl->stopThread && comb.get() && n--)
  {
    if (strHash == md5.digestString(comb.get()))
    {
      std::cout << "Gotcha!!!  " << comb.get() << std::endl;
      message msg;
      msg.found = true;
      msg.duration = 0;
      msg.attackNum = 0;
      threadControl->messageQueue.push(msg);
      return;
    }
    
    comb.next();
  }

  const auto elapsed = system_clock::now() - t0;

  message msg;
  msg.found = false;
  msg.duration = elapsed.count() * nanoseconds::period::num / nanoseconds::period::den;
  msg.attackNum = (attackNum - n - 1);
  threadControl->messageQueue.push(msg);
}

// -----------------------------------------------------------------------------

void check (bool condition, std::string errmsg)
{
  if (!condition)
  {
    std::cout << errmsg << endl;
    exit(1);
  }
}

int main(int argc, char** argv)
{
  test();

  if (5 != argc)
  {
    cout << "Usage: application <minlen> <maxlen> <interval>[:<interval>] <md5hash>" << endl;
    cout << "  minlen - mininal length of input string" << endl;
    cout << "  manlen - maximum length of input string" << endl;
    cout << "  interval - ASCII symbol interval (decimal), " << endl
         << "             e.g. 65-90 or 65-63:97-122 or 65-77:77-90:97-122 etc." << endl;
    cout << "  md5hash - md5 hash string to attack" << endl;
    exit(1);
  }
  
  // Parse command line args
  const int startNum = stoi(argv[1]);
  const int endNum = stoi(argv[2]);
  check(startNum <= endNum, "Invalid min/max length");
  
  const string_vector tokens = split(argv[3], ':');
  const string strMd5(argv[4]);

  // Create symbol array to combinate.
  string symbols;
  {
    for (auto t : tokens)
    {
      auto r = split(t, '-');
      check(r.size() == 2, string("Invalid symbol interval:") + t);
      const auto r0 = stoi(r[0]);
      const auto r1 = stoi(r[1]);
      check(r0 <= r1, string("Invalid symbol interval:") + t);
      for (auto c = r0; r1 >= c; ++c)
      {
        symbols.push_back(static_cast<char>(c));
      }
    }
    // Remove duplicates
    sort(symbols.begin(), symbols.end());
    auto it = std::unique(symbols.begin(), symbols.end()); 
    symbols.resize(std::distance(symbols.begin(), it));
  }
  
  // Start attacking threads
  {
    MD5 md5;

    cout << "std::thread::hardware_concurrency = " << std::thread::hardware_concurrency() << endl;

    const unsigned int maxThreadNum = std::thread::hardware_concurrency() > 0 ? std::thread::hardware_concurrency() : 1;
    unsigned int threadNum = maxThreadNum;
    size_t threadStep = 1000000;
    Combinations combinations(symbols.c_str(), startNum, endNum);
    
    thread_control threadControl;
    
    message msg;
    msg.found = false;
    msg.duration = 0;
    msg.attackNum = 0;

    cout << "Starting " << threadNum << " threads." << endl;
    do
    {
      // Adjust thread's run interval.
      if (msg.duration > 2)
      {
        threadStep = DesiredThreadInterval * msg.attackNum / msg.duration;
      }
      else if (msg.attackNum)
      {
        threadStep = msg.attackNum * 2;
      }
        
      // Start new thread
      //cout << "Starting a thread, threadStep = " << threadStep << ", start = " << combinations.get() << endl;
      thread(attack_thread, strMd5, combinations, threadStep, &threadControl).detach();
      --threadNum;
      combinations += threadStep;
      
      if (0 == threadNum)
      {
        // No more space to run a thread. Wait for any thread completion.
        threadControl.messageQueue.wait_and_pop(msg);
        ++threadNum;
        //cout << "Not found..., attackNum = " << msg.attackNum;
        //cout << ", duration (in seconds): " << msg.duration << endl;
      }
    }    
    while (!msg.found && combinations.get()) ;

    // Wait for threads' completion.
    threadNum = maxThreadNum - threadNum;
    //cout << "Waiting for completion " << threadNum << " threads." << endl;
    while (threadNum--)
    {
        if (msg.found) 
        {
          // Stop all threads if found.
          threadControl.stopThread = true;
        }
        threadControl.messageQueue.wait_and_pop(msg);
    }

    //cout << "Threads completed." << endl;
  }
}
